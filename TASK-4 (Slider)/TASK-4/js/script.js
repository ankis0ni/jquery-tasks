$(document).ready(function () {
  $("#on").attr("disabled", true);                                  //initially on button will be disable
  $(document).on("click", "#next", function ()                      //next arrow is clicked
  {
    if ($(".slider-images.active").index() >= $(".slider-images").length - 1) {         //to find the  index
      $(".slider-images").removeClass("active").first().addClass("active");
    } else {
      $(".slider-images.active").removeClass("active").next().addClass("active");
    }
    $(".dot").removeClass("show").eq($(".slider-images.active").index()).addClass("show");
  });
  $(document).on("click", "#prev", function () {                      //previous button clicked here
    if ($(".slider-images.active").index() == 0) {                    //to find the previous index
      $(".slider-images").removeClass("active").last().addClass("active");
    } else {
      $(".slider-images.active").removeClass("active").prev().addClass("active");
    }
    $(".dot").removeClass("show").eq($(".slider-images.active").index()).addClass("show");
  });
  var autoslider = setInterval(function () {                          // this is to start the auto-slider
    $("#next").trigger("click");
  }, 5000);
  $(document).on("click", ".dot", function () {                        //function for dots
    $(".slider-images").removeClass("active").eq($(this).index()).addClass("active");
    $(this).addClass("show").siblings().removeClass("show");
  });
  $(document).on("click", "#off", function () {              //here the sldier stops by clicking the off button
    clearInterval(autoslider);
    $("#on").attr("disabled", false);
  });
  $(document).on("click", "#on", function () {                          //the on-button function is here
    autoslider = setInterval(function () {
      $("#next").trigger("click");
    }, 5000);
    $("#on").attr("disabled", true);
  });
});
